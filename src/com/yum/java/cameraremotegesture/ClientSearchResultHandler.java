package com.yum.java.cameraremotegesture;
import java.util.logging.Logger;

import com.example.sony.cameraremote.ServerDevice;
import com.example.sony.cameraremote.SimpleSsdpClient.SearchResultHandler;


class ClientSearchResultHandler implements SearchResultHandler{
	
	ServerDevice mTargetServer = null; 
	
	@Override
	public void onDeviceFound(final ServerDevice device) {
		// Called by non-UI thread.				mStatusString = ">> Search device found: " + device.getFriendlyName(); 
		Logger.getGlobal().info(
				">> Search device found: " + device.getFriendlyName());
		mTargetServer = new ServerDevice();
		mTargetServer = device;
	}

	public ServerDevice getTargetDevice(){
		return mTargetServer;
	}
	
	@Override
	public void onFinished() {
		// Called by non-UI thread.
		Logger.getGlobal().info( ">> Search finished.");
		
	}

	@Override
	public void onErrorFinished() {
		// Called by non-UI thread.
		Logger.getGlobal().info( ">> Search Error finished.");
		
	}
}



