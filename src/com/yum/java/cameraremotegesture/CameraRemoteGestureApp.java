package com.yum.java.cameraremotegesture;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URI;
import java.net.URL;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.applet.Applet;

import javax.websocket.ClientEndpointConfig;
import javax.websocket.DeploymentException;
import javax.websocket.Endpoint;
import javax.websocket.EndpointConfig;
import javax.websocket.MessageHandler;
import javax.websocket.Session;

import org.glassfish.tyrus.client.ClientManager;
import org.glassfish.tyrus.server.Server;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;





import com.example.sony.cameraremote.ServerDevice;
import com.example.sony.cameraremote.SimpleCameraEventObserver;
import com.example.sony.cameraremote.SimpleRemoteApi;
import com.example.sony.cameraremote.SimpleSsdpClient;
import com.example.sony.cameraremote.SimpleSsdpClient.SearchResultHandler;
import com.example.sony.cameraremote.utils.SimpleHttpClient;
import com.yum.java.tyrus.EchoServerEndpoint;
import com.yum.java.tyrus.WebSocketStandaloneServer;




public class CameraRemoteGestureApp {

	/**
	 * @param args
	 */
	
	private static final String SEARCH = "Search";
	private static final String TAKEPICTURE = "TakePicture";
	
	private static SimpleSsdpClient mSsdpClient;
	private static ServerDevice mTargetServer;
	private static SimpleRemoteApi mRemoteApi;
	private final static Set<String> mAvailableApiSet = new HashSet<String>();
	private static SimpleCameraEventObserver mEventObserver;
	
	private static WebSocketStandaloneServer mWebSocketServer;
	private static Session session;

	
	private CameraRemoteGestureApp() {
	}
	

	
	public static void main(String[] args) throws DeploymentException, IOException, InterruptedException{
		
		startWebsocketServer();
		waitCommand();
	}
	
	public static void startWebsocketServer(){
		
		// Start Websocket server to receive message from the webbrowser
		mWebSocketServer = new WebSocketStandaloneServer();		
		new Thread(){
			@Override
            public void run() {
				mWebSocketServer.runServer();
			}
		}.start();
		
	}

	public static void waitCommand(){
		// Start Websocket server to receive message from the webbrowser
		System.out.println("Waiting for...") ;
		runClient();
	}
	
	public static void runClient(){
	       try {
	            final ClientEndpointConfig cec = ClientEndpointConfig.Builder.create().build();
	            ClientManager client = ClientManager.createClient();
	            session = client.connectToServer(new Endpoint() {
	                @Override
	                public void onOpen(final Session session, EndpointConfig config) {
	                    session.addMessageHandler(new MessageHandler.Whole<String>() {
						    @Override
						    public void onMessage(String message) {
						        System.out.println("Received message: " + message);
						        if (message.equals(SEARCH)){
						        	searchDevices();
						        }
						        else if(message.equals(TAKEPICTURE)){
						        	takeAndFetchPicture();
						        }
						        else{
						        	System.out.println("Invalid message") ;
						        }
						    }
						});
						//session.getBasicRemote().sendText(SENT_MESSAGE);
	                }
	            }, cec, new URI("ws://localhost:8025/websockets/echo"));
	            //session.close();
	        } catch (Exception e) {
	            e.printStackTrace();
	        }

	}
	
	   // Take a picture and retrieve the image data.
    private static void takeAndFetchPicture() {

        new Thread() {

            @Override
            public void run() {
                try {
                    JSONObject replyJson = mRemoteApi.actTakePicture();
                    JSONArray resultsObj = replyJson.getJSONArray("result");
                    JSONArray imageUrlsObj = resultsObj.getJSONArray(0);
                    String postImageUrl = null;
                    if (1 <= imageUrlsObj.length()) {
                        postImageUrl = imageUrlsObj.getString(0);
                    }
                    if (postImageUrl == null) {
                    	Logger.getGlobal().info(
                                "takeAndFetchPicture: post image URL is null.");
                         return;
                    }
                    
                    URL url = new URL(postImageUrl);
                    InputStream istream = new BufferedInputStream(url.openStream());
                	Logger.getGlobal().info( "url:"+url.getPath() );
                	String fileName = "C:/Users/0000128356"+ url.getPath(); //getPath = /postview/(unique name)
                    File saveFile = new File(fileName);
            	    FileOutputStream ostream = new FileOutputStream(saveFile);
            	    int c;
            	    while((c =istream.read()) != -1) ostream.write((byte) c);
            	    istream.close();
            	    ostream.close();
            	    
            	    System.out.println("Send message:"+fileName);
            	    session.getBasicRemote().sendText(fileName);
            	    
                } catch (IOException e) {
                	Logger.getGlobal().info( "IOException while closing slicer: " + e.getMessage());
                } catch (JSONException e) {
                	Logger.getGlobal().info( "JSONException while closing slicer");
                } finally {
                }
            }
        }.start();
    }
	
	public static void searchDevices() {

		Logger.getGlobal().setLevel(Level.INFO);
		
        mTargetServer = new ServerDevice();		
		mSsdpClient = new SimpleSsdpClient();
			
		//ClientSearchResultHandler mSearchResultHandler;
		//mSearchResultHandler =  new ClientSearchResultHandler();
		//mSsdpClient.search( mSearchResultHandler );
		mSsdpClient.search(new SimpleSsdpClient.SearchResultHandler() {
			
			@Override
			public void onDeviceFound(final ServerDevice device) {
				// Called by non-UI thread.				mStatusString = ">> Search device found: " + device.getFriendlyName(); 
				Logger.getGlobal().info(
						">> Search device found: " + device.getFriendlyName());
				mTargetServer = device;
				openConnection();
			}

			@Override
			public void onFinished() {
				// Called by non-UI thread.
				Logger.getGlobal().info( ">> Search finished.");
				
			}

			@Override
			public void onErrorFinished() {
				// Called by non-UI thread.
				Logger.getGlobal().info( ">> Search Error finished.");
				
			}
		});
		
	}
	
    // Open connection to the camera device to start monitoring Camera events
    // and showing liveview.
    private static void openConnection() {
	        
        new Thread() {

            @Override
            public void run() {
            	Logger.getGlobal().info( "openConnection(): exec.");
                try {
                    JSONObject replyJson = null;
                    if (mTargetServer.getDDUrl() != null){
                    	mRemoteApi = new SimpleRemoteApi(mTargetServer);
                		mEventObserver = new SimpleCameraEventObserver(mRemoteApi);

                    	// getAvailableApiList
	                    replyJson = mRemoteApi.getAvailableApiList();
	                    loadAvailableApiList(replyJson);
	
	                    // check version of the server device
	                    if (isApiAvailable("getApplicationInfo")) {
	                    	Logger.getGlobal().info( "openConnection(): getApplicationInfo()");
	                        replyJson = mRemoteApi.getApplicationInfo();
	                        if (!isSupportedServerVersion(replyJson)) {
	                        	Logger.getGlobal().info( "NotSupportedVersion");
	                        	return;
	                        }
	                    } else {
	                        // never happens;
	                        return;
	                    }
	
	                    // startRecMode if necessary.
	                    if (isApiAvailable("startRecMode")) {
	                    	Logger.getGlobal().info( "openConnection(): startRecMode()");
	                        replyJson = mRemoteApi.startRecMode();
	
	                        // Call again.
	                        replyJson = mRemoteApi.getAvailableApiList();
	                        loadAvailableApiList(replyJson);
	                    }
	
	                    // getEvent start
	                    if (isApiAvailable("getEvent")) {
	                    	Logger.getGlobal().info("openConnection(): EventObserver.start()");
	                        mEventObserver.start();
	                    }
	
	                    // Liveview start
	                    if (isApiAvailable("startLiveview")) {
	                    	Logger.getGlobal().info( "openConnection(): LiveviewSurface.start()");
	                        //mLiveviewSurface.start();
	                    }
	
	                    // prepare UIs
	                    if (isApiAvailable("getAvailableShootMode")) {
	                    	Logger.getGlobal().info(
	                                "openConnection(): prepareShootModeRadioButtons()");
	                       // prepareShootModeRadioButtons();
	                        // Note: hide progress bar on title after this calling.
	                    }
	
	                    Logger.getGlobal().info( "openConnection(): completed.");
	                    session.getBasicRemote().sendText( mTargetServer.getFriendlyName());
                	}	
                } catch (IOException e) {
                	Logger.getGlobal().info( "openConnection: IOException: " + e.getMessage());

                }
            }
        }.start();
    }

    // Close connection to stop monitoring Camera events and showing liveview.
    private void closeConnection() {
        new Thread() {

            @Override
            public void run() {
            	Logger.getGlobal().info( "closeConnection(): exec.");
                try {
                    // Liveview stop
                	Logger.getGlobal().info( "closeConnection(): LiveviewSurface.stop()");
                    //mLiveviewSurface.stop();

                    // getEvent stop
                    Logger.getGlobal().info( "closeConnection(): EventObserver.stop()");
                    //mEventObserver.stop();

                    // stopRecMode if necessary.
                    if (isApiAvailable("stopRecMode")) {
                    	Logger.getGlobal().info( "closeConnection(): stopRecMode()");
                        mRemoteApi.stopRecMode();
                    }

                    Logger.getGlobal().info("closeConnection(): completed.");
                } catch (IOException e) {
                	Logger.getGlobal().info(
                            "closeConnection: IOException: " + e.getMessage());
                }
            }
        }.start();
    }
    // Retrieve a list of APIs that are available at present.
    private static void loadAvailableApiList(JSONObject replyJson) {
        synchronized (mAvailableApiSet) {
            mAvailableApiSet.clear();
            try {
                JSONArray resultArrayJson = replyJson.getJSONArray("result");
                JSONArray apiListJson = resultArrayJson.getJSONArray(0);
                for (int i = 0; i < apiListJson.length(); i++) {
                    mAvailableApiSet.add(apiListJson.getString(i));
                }
            } catch (JSONException e) {
            	Logger.getGlobal().info("loadAvailableApiList: JSON format error.");
            }
        }
    }

    // Check if the indicated API is available at present.
    private static boolean isApiAvailable(String apiName) {
        boolean isAvailable = false;
        synchronized (mAvailableApiSet) {
            isAvailable = mAvailableApiSet.contains(apiName);
        }
        return isAvailable;
    }

    // Check if the version of the server is supported in this application.
    private static boolean isSupportedServerVersion(JSONObject replyJson) {
        try {
            JSONArray resultArrayJson = replyJson.getJSONArray("result");
            String version = resultArrayJson.getString(1);
            String[] separated = version.split("\\.");
            int major = Integer.valueOf(separated[0]);
            if (2 <= major) {
                return true;
            }
        } catch (JSONException e) {
        	Logger.getGlobal().info( "isSupportedServerVersion: JSON format error.");
        } catch (NumberFormatException e) {
        	Logger.getGlobal().info("isSupportedServerVersion: Number format error.");
        }
        return false;
    }
    

}
