package com.yum.java.tyrus;

import org.glassfish.tyrus.server.Server;


public class WebSocketStandaloneServer {

    private static final String HOST = "localhost";
    private static final int WEB_SOCKET_PORT = 8025;
    private static final String WEB_SOCKET_CONTEXT = "/websockets";
    private static Server mServer;
    
    
    public static void runServer() {
        mServer = new Server(HOST, WEB_SOCKET_PORT, WEB_SOCKET_CONTEXT, EchoServerEndpoint.class);
        try {
            mServer.start();
            System.out.println("Please press a key to stop the server.");
            System.in.read();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            mServer.stop();
        }
    }
}
