package com.yum.java.tyrus;

import javax.websocket.CloseReason;
import javax.websocket.EndpointConfig;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;


@ServerEndpoint(value = "/echo")
public class EchoServerEndpoint {

    @OnOpen
    public void onOpen(Session session, EndpointConfig config) throws IOException {
        System.out.println("onOpen: " + session + ", config:" + config);
    }

    @OnClose
    public void onClose(Session session, CloseReason reason) throws IOException {
        System.out.println("onClose: " + session + ", reason:" + reason);
    }

    @OnMessage
    public void onMessage(Session session, String message) {
        System.out.println("onMessage: " + session + ", message: " + message);
        for (Session s : session.getOpenSessions()) {
            s.getAsyncRemote().sendText(message);
        }
    }

    @OnError
    public void onError(Session session, Throwable t) {
        System.out.println("onError: " + session + ", throwable:" + t);
    }
}
